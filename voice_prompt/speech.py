from typing import Optional
import time
import os
import azure.cognitiveservices.speech as speechsdk


class SpeechError(Exception):
    pass


class SpeechNoMatchError(SpeechError):
    pass


class SpeechCanceledError(SpeechError):
    pass


class SpeechStyles:
    NEWSCAST = 'newscast'
    CUSTOMER_SERVICE = 'customerservice'
    CHAT = 'chat'
    CHEERFUL = 'cheerful'
    EMPATHETIC = 'empathetic'


class Speech:
    RETRY_PROMPT = "I'm sorry, I didn't understand that. Can you say it again?"
    SSML_TEMPLATE = """
        <speak version="1.0" xmlns="http://www.w3.org/2001/10/synthesis" xmlns:mstts="https://www.w3.org/2001/mstts" xml:lang="en-US">
          <voice name="{voice}">
            <mstts:express-as style="{expression}">
              {text}
            </mstts:express-as>
          </voice>
        </speak>
    """

    def __init__(self, voice: str='en-US-AriaNeural', style: str=SpeechStyles.CHAT, api_key: Optional[str]=None, region: Optional[str]=None):
        api_key = api_key or os.environ['AZURE_SPEECH_API_KEY']
        region = region or os.environ['AZURE_SPEECH_REGION']

        self.voice = voice
        self.style = style

        self.speech_config = speechsdk.SpeechConfig(subscription=api_key, region=region)
        self.speech_recognizer = speechsdk.SpeechRecognizer(speech_config=self.speech_config)
        self.speech_synthesizer = speechsdk.SpeechSynthesizer(speech_config=self.speech_config)

    def speech_to_text(self):
        while True:
            result = self.speech_recognizer.recognize_once()

            if result.reason == speechsdk.ResultReason.RecognizedSpeech:
                return result.text
            elif result.reason == speechsdk.ResultReason.NoMatch:
                continue
            elif result.reason == speechsdk.ResultReason.Canceled:
                cancellation_details = result.cancellation_details
                if cancellation_details.reason == speechsdk.CancellationReason.Error:
                    raise SpeechCanceledError(f'Speech recognition error: {cancellation_details.error_details}')
                else:
                    raise SpeechCanceledError(f'Speech recognition canceled: {cancellation_details.reason}')

    def text_to_speech(self, text: str, style: Optional[str]=None, voice: Optional[str]=None):
        ssml_text = self.SSML_TEMPLATE.format(voice=voice or self.voice, expression=style or self.style, text=text)
        result = self.speech_synthesizer.speak_ssml(ssml_text)

        if result.reason == speechsdk.ResultReason.Canceled:
            cancellation_details = result.cancellation_details
            if cancellation_details.reason == speechsdk.CancellationReason.Error:
                raise SpeechCanceledError(f'Speech synthesis error: {cancellation_details.error_details}')
            else:
                raise SpeechCanceledError(f'Speech synthesis canceled: {cancellation_details.reason}')

    def prompt(self, text: str):
        self.text_to_speech(text)

        while True:
            try:
                return self.speech_to_text()
            except SpeechError:
                self.text_to_speech(self.RETRY_PROMPT)


if __name__ == '__main__':
    speech = Speech()
    speech.text_to_speech('Welcome to the CHEM 101 titration experiment')
