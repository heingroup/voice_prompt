from typing import Union, Dict, Optional, Callable, List
import re
import sys
import inspect
from voice_prompt.speech import Speech


"""
Prompt('What is your name?', {'.*': lambda name: f'hello {name}!'})
Prompt('Would you like to continue?', {
    'yes': Prompt('How are you?', lambda mood: f'I am glad you are {mood}'),
    'no': 'Thank you'
})
"""

ResponseActionType = Union[str, 'Promptable', Callable[[str], 'ResponseActionType']]
ResponsesType = Union[ResponseActionType, Dict[str, ResponseActionType]]


class PromptError(Exception):
    pass


class PromptInvalidResponseAction(PromptError):
    pass


class PromptInvalidResponse(PromptError):
    pass


class Promptable:
    INVALID_RESPONSE = "I didn't understand, please repeat"

    def __init__(self):
        self.root_prompt: Optional[Promptable] = None
        self.loop_stack: List[Loop] = []

    def strip_response(self, response) -> str:
        return response.lower().rstrip('.!?')

    def prompt(self, speech: Optional[Speech]=None, root_prompt: Optional['Promptable']=None):
        self.root_prompt = self if root_prompt is None else root_prompt

    def enter_loop(self, loop: 'Loop'):
        if self.root_prompt is None:
            return

        self.root_prompt.loop_stack.append(loop)

    def exit_loop(self, pop: bool=False):
        if self.root_prompt is None or len(self.root_prompt.loop_stack) <= 0:
            return

        current_loop = self.root_prompt.loop_stack[-1]
        current_loop.running = False

        if pop:
            self.root_prompt.loop_stack.pop()

    def handle_responses(self, speech: Speech, response: str, responses: ResponsesType):
        stripped_response = self.strip_response(response)
        print(response, stripped_response)

        if not isinstance(responses, dict):
            return self.handle_response_action(speech, stripped_response, responses)

        for response_re, response_action in responses.items():
            pattern = f'^{response_re}$'
            match = re.match(pattern, response) or re.match(pattern, stripped_response)
            if match:
                if len(match.groups()) > 0:
                    responses = [self.strip_response(resp or '') for resp in match.groups()]
                    return self.handle_response_action(speech, responses, response_action)

                return self.handle_response_action(speech, stripped_response, response_action)

    def handle_response_action(self, speech: Speech, response: Union[str, List[str]], response_action: ResponseActionType):
        if isinstance(response_action, str):
            speech.text_to_speech(response_action)
        elif isinstance(response_action, Promptable):
            return response_action.prompt(speech, self.root_prompt)
        elif callable(response_action):
            try:
                num_args = len(inspect.signature(response_action).parameters)
            except ValueError:
                num_args = 0

            if isinstance(response, list):
                action_value = response_action(*response[0:num_args])
            elif num_args == 0:
                action_value = response_action()
            elif num_args == 1:
                action_value = response_action(response)
            else:
                action_value = response_action(response, speech)

            if action_value is not None:
                self.handle_response_action(speech, response, action_value)
        else:
            raise PromptInvalidResponseAction(f'Invalid response action, must be a str, Promptable or callable: {response_action}')


class Say(Promptable):
    def __init__(self, text: str, action: Optional[ResponseActionType]=None):
        super().__init__()
        self.text = text
        self.action = action

    def prompt(self, speech: Optional[Speech]=None, root_prompt: Optional['Promptable']=None):
        super().prompt(speech, root_prompt)

        if speech is None:
            speech = Speech()

        speech.text_to_speech(self.text)

        if self.action is not None:
            self.handle_response_action(speech, '', self.action)


class Listen(Promptable):
    def __init__(self, responses: ResponsesType, add_loop_exit: bool=False, loop_exit_command: str='exit'):
        super().__init__()
        self.responses = responses

        if add_loop_exit:
            self.responses[loop_exit_command] = self.exit_loop

    def prompt(self, speech: Optional[Speech] = None, root_prompt: Optional['Promptable'] = None):
        super().prompt(speech, root_prompt)

        if speech is None:
            speech = Speech()

        while True:
            try:
                response = speech.speech_to_text()
                print('listen response:', response)
                return self.handle_responses(speech, response, self.responses)
            except PromptInvalidResponse:
                speech.text_to_speech(self.INVALID_RESPONSE)


class Loop(Promptable):
    def __init__(self, action: ResponseActionType, _while: Optional[Callable]=None, prompt: Optional[str]=None):
        super().__init__()
        self.action = action
        self.running = False
        self._while = _while if _while is not None else lambda: True
        self.prompt_text = prompt

    def prompt(self, speech: Optional[Speech] = None, root_prompt: Optional['Promptable'] = None):
        super().prompt(speech, root_prompt)

        if speech is None:
            speech = Speech()

        self.enter_loop(self)

        self.running = True
        first = True
        while self.running and self._while():
            if first and self.prompt_text:
                speech.text_to_speech(self.prompt_text)

            self.handle_response_action(speech, '', self.action)
            first = False

        self.exit_loop(pop=True)


class Prompt(Promptable):
    def __init__(self, prompt_text: str, responses: Union[ResponseActionType, Dict[str, ResponseActionType]]={}):
        super().__init__()
        self.prompt_text = prompt_text
        self.responses = responses

    def prompt(self, speech: Optional[Speech] = None, root_prompt: Optional['Promptable'] = None):
        super().prompt(speech, root_prompt)

        if speech is None:
            speech = Speech()

        while True:
            try:
                response = speech.prompt(self.prompt_text)
                print('prompt response: ', response)
                return self.handle_responses(speech, response, self.responses)
            except PromptInvalidResponse:
                speech.text_to_speech(self.INVALID_RESPONSE)


class Sequence(Promptable):
    def __init__(self, *prompts: ResponseActionType):
        super().__init__()
        self.prompts = prompts

    def prompt(self, speech: Optional[Speech] = None, root_prompt: Optional['Promptable'] = None):
        super().prompt(speech, root_prompt)

        if speech is None:
            speech = Speech()

        for prompt in self.prompts:
            self.handle_response_action(speech, '', prompt)


class Script:
    @classmethod
    def run_script(cls, speech: Optional[Speech]=None):
        script = cls()
        script._run(speech)

    def __init__(self, root_prompt: Optional[Promptable]=None):
        self.root_prompt = root_prompt or self.prompt()

    def enter_loop(self, loop: Loop):
        self.root_prompt.enter_loop(loop)

    def exit_loop(self):
        self.root_prompt.exit_loop()

    def prompt(self) -> Promptable:
        return Sequence()

    def _run(self, speech: Optional[Speech]=None):
        self.root_prompt.prompt(speech)
