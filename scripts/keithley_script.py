from typing import Optional
from north_utils.test import float_equal
from north_devices.power_supplies.keithley import KeithleyPS, KeithleyPSChannel
from voice_prompt.prompt import Script, Say, Loop, Sequence, Listen, Prompt, Promptable, PromptInvalidResponse

PORT = 'COM4'


class KeithleyScript(Script):
    COMMANDS = ['exit', 'help', 'select channel', 'get voltage', 'get current', 'set voltage', 'set current', 'on', 'off']

    def __init__(self, root_prompt: Optional[Promptable]=None):
        super().__init__(root_prompt)
        self.power_supply = KeithleyPS(f'ASRL{PORT}')
        self.channel_index = 0

    @property
    def channel(self) -> KeithleyPSChannel:
        return self.power_supply.channels[self.channel_index]

    def select_channel(self, channel: str):
        try:
            channel_index = int(channel) - 1
        except ValueError:
            raise PromptInvalidResponse()

        if channel_index >= len(self.power_supply.channels):
            return f'Available channels are 1 through {len(self.power_supply.channels)}'

        self.channel_index = channel_index
        return f'Channel {channel} selected'

    def get_voltage(self):
        return f'{self.channel.voltage} volts'

    def get_current(self):
        return f'{self.channel.current} amps'

    def measure_voltage(self):
        return f'{self.channel.measured_voltage} volts'

    def measure_current(self):
        return f'{self.channel.measured_current} amps'

    def set_voltage(self, voltage: str, channel=None):
        try:
            channel = self.power_supply.channels[int(channel) - 1] if channel is not None else self.channel
            channel.set(voltage=float(voltage))
        except (ValueError, IndexError):
            raise PromptInvalidResponse()

        return f'Voltage set to {voltage} volts'

    def set_current(self, current: str, channel=None):
        try:
            channel = self.power_supply.channels[int(channel) - 1] if channel is not None else self.channel
            channel.set(current=float(current))
        except (ValueError, IndexError):
            raise PromptInvalidResponse()

        return f'Current set to {current} amps'

    def set_voltage_and_current(self, voltage: str, current: str, channel=None):
        self.set_voltage(voltage, channel)
        self.set_current(current, channel)
        return f'Set to {voltage} volts and {current} amps'

    def on(self, channel=None):
        try:
            channel = self.power_supply.channels[int(channel) - 1] if channel is not None else self.channel
            channel.on()
            return f'Channel on'
        except (ValueError, IndexError):
            raise PromptInvalidResponse()

    def off(self, channel=None):
        try:
            channel = self.power_supply.channels[int(channel) - 1] if channel is not None else self.channel
            channel.off()
        except (ValueError, IndexError):
            raise PromptInvalidResponse()

        return f'Channel off'

    def help(self):
        return f"Available actions are: {', '.join(self.COMMANDS[:-1])} and {self.COMMANDS[-1]}"

    def command_listen(self) -> Promptable:
        return Listen({
            'exit': self.exit_loop,
            'help': self.help,
            'select channel': Prompt('Which channel?', lambda channel: self.select_channel(channel)),
            '(?:select )?channel (\d)': lambda channel: self.select_channel(channel),
            'get voltage': self.get_voltage,
            'measure voltage': self.measure_voltage,
            'set voltage': Prompt('How many volts?', lambda voltage: self.set_voltage(voltage)),
            '(?:set )?voltage (?:\w+) ([\d\.]*)(?: volts?)?': lambda voltage: self.set_voltage(voltage),
            '(?:set )?voltage (?:\w+) ([\d\.]*)(?: volts?)? (?:\w+) channel (\d)': lambda voltage, channel: self.set_voltage(voltage, channel),
            'get current': self.get_current,
            'measure current': self.measure_current,
            'set current': Prompt('How many amps?', lambda current: self.set_current(current)),
            '(?:set )?current (?:\w+) ([\d\.]*)(?: amps?)?': lambda current: self.set_current(current),
            '(?:set )?current (?:\w+) ([\d\.]*)(?: amps?)? (?:\w+) channel (\d)': lambda current, channel: self.set_current(current, channel),
            '(?:set )?voltage (?:\w+) ([\d\.]*)(?: volts?)? (?:\w+) current (?:\w+) ([\d\.]*)(?: amps?)?': lambda voltage, current: self.set_voltage_and_current(voltage, current),
            '(?:set )?current (?:\w+) ([\d\.]*)(?: amps?)? (?:\w+) voltage (?:\w+) ([\d\.]*)(?: volts?)?': lambda current, voltage: self.set_voltage_and_current(voltage, current),
            '(?:\w+ )?on': lambda: self.on(),
            '(?:\w+ )?on channel (\d)': lambda channel: self.on(channel),
            '(?:\w+ )?off': lambda: self.off(),
            '(?:\w+ )?off channel (\d)': lambda channel: self.off(channel),
        })

    def command_loop(self) -> Promptable:
        return Loop(self.command_listen())

    def prompt(self) -> Promptable:
        return Sequence(
            Say(f'Welcome to the Keithley-compatible power supply control script, {self.help()}'),
            self.command_loop(),
            Say('Goodbye!')
        )


class KeithleyTutorial(Script):
    HELP = 'Available commands are: help, exit, start and control'

    def __init__(self, root_prompt: Optional[Promptable]=None):
        super().__init__(root_prompt)
        self.keithley_script = KeithleyScript(self.root_prompt)
        self.channels_on = []
        self.keithley_script.set_voltage_and_current(0.1, 0.5, 1)
        self.keithley_script.set_voltage_and_current(0.1, 0.5, 2)

    def update_channels_on(self) -> bool:
        self.channels_on = []

        for index in range(len(self.keithley_script.power_supply.channels)):
            channel = self.keithley_script.power_supply.channels[index]
            if channel.output:
                self.channels_on.append(str(index + 1))

        return len(self.channels_on) > 0

    def ask_to_turn_off_channels(self):
        self.update_channels_on()
        if len(self.channels_on) > 0:
            return Sequence(
                Say(f'Please turn off channels {", ".join(self.channels_on)}'),
                Loop(
                    lambda: self.keithley_script.command_listen(),
                    _while=lambda: self.update_channels_on()
                ),
                Say('Thank you')
            )

    def tutorial(self) -> Promptable:
        return Sequence(
            lambda: self.ask_to_turn_off_channels(),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Set channel 1 to 5 volts',
                _while=lambda: not float_equal(self.keithley_script.power_supply.channels[0].voltage, 5.0, diff=0.01)
            ),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Set channel 1 to 0.05 amps',
                _while=lambda: not float_equal(self.keithley_script.power_supply.channels[0].current, 0.05, diff=0.01)
            ),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Good job! Now turn on channel 1',
                _while=lambda: not self.keithley_script.power_supply.channels[0].output
            ),
            Say('The light should be glowing dimly'),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Set channel 2 to 1 volt',
                _while=lambda: not float_equal(self.keithley_script.power_supply.channels[1].voltage, 1.0, diff=0.1)
            ),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Thanks! Now turn on channel 2',
                _while=lambda: not self.keithley_script.power_supply.channels[1].output
            ),
            Say('The multimeter should be reading 1.0 volts'),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Set channel 2 to 12.5 volt',
                _while=lambda: not float_equal(self.keithley_script.power_supply.channels[1].voltage, 12.5, diff=0.1)
            ),
            Say('The multimeter should be reading 12.5 volts'),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Set channel 1 to 0.1 amps',
                _while=lambda: not float_equal(self.keithley_script.power_supply.channels[0].current, 0.1, diff=0.01)
            ),
            Say('The light should be brighter after you increased the current'),
            Loop(
                lambda: self.keithley_script.command_listen(),
                prompt='Try changing the current on channel 1 to see how the brightness changes! Say "measure voltage" '
                       'to see what the output voltage is for the different current values! Say "exit" when you are '
                       'finished.',
            ),
            Say('Tutorial complete!')
        )

    def prompt(self) -> Promptable:
        return Sequence(
            Say(
                'Welcome to the Keithley-compatible power supply tutorial! Please connect your devices to the power '
                'supply as shown in the diagram, then say "start" to begin the tutorial or "control" to control the '
                'power supply!'
            ),
            Loop(
                Listen({
                    'start': Sequence(self.tutorial(), 'Returned to tutorial menu'),
                    'control': Sequence(
                        'Say "exit" to return to the tutorial menu',
                        lambda: self.keithley_script.command_loop(),
                        'Returned to tutorial menu'
                    ),
                    'help': self.HELP,
                }, add_loop_exit=True)
            ),
            Say('Goodbye!')
        )


# KeithleyScript.run_script()
KeithleyTutorial.run_script()