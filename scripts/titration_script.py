import os

import time
from newera.new_era import NewEraPeristalticPumpInterface
from north_devices.pumps.tecan_cavro import TecanCavro
from ftdi_serial import Serial
from ika.magnetic_stirrer import MagneticStirrer
from rpc_gateway.client import Client

os.environ['AZURE_SPEECH_API_KEY'] = 'c0f261fb981043819cee3f29ed0a712b'
os.environ['AZURE_SPEECH_REGION'] = 'eastus'

from voice_prompt.prompt import Script, Say, Loop, Sequence, Listen, Prompt, Promptable


class TestScript(Script):
    HELP = 'available actions are: help, dispense, start, stop and exit',
    total_volume_dispensed_analyte = 0
    total_volume_dispensed_titrant = 0
    total_solution_in_flask = 0
    remaining_titrant_stock = 0
    remaining_analyte_stock = 0

    # def dispense_peristaltic(self, amount):
    #     pump_port = 'COM3'  # check on your own system
    #     ne_pump = NewEraPeristalticPumpInterface(port=pump_port)
    #     ne_pump.set_direction('dispense')
    #
    #     if amount >= 50:
    #         ne_pump.set_rate(10)  # in ml/min
    #         ne_pump.start()  # start the pump
    #         time.sleep(amount * 60 / 10)
    #         ne_pump.stop()  # stop the pump
    #
    #     elif amount >= 20:
    #         ne_pump.set_rate(7)  # in ml/min
    #         ne_pump.start()  # start the pump
    #         time.sleep(amount * 60 / 7)
    #         ne_pump.stop()  # stop the pump
    #
    #     else:
    #         ne_pump.set_rate(5)  # in ml/min
    #         ne_pump.start()  # start the pump
    #         time.sleep(amount * 60 / 5)
    #         ne_pump.stop()  # stop the pump_port

    def prime_syringe_pump(self):
        syringe_pump_1.dispense_ml(volume_ml=3,
                                   from_port=pump_stock_position,
                                   to_port=pump_waste_position,
                                   velocity_ml=pump_default_velocity
                                   )
        syringe_pump_2.dispense_ml(volume_ml=3,
                                   from_port=pump_stock_position,
                                   to_port=pump_waste_position,
                                   velocity_ml=pump_default_velocity
                                   )


    def dispense_analyte(self, amount):
        if amount >= 100:
            return Say('The input volume is too high.')
        else:
            self.total_solution_in_flask += int(amount)
            self.remaining_analyte_stock -= int(amount)
            self.total_volume_dispensed_analyte += int(amount)
            syringe_pump_1.dispense_ml(volume_ml=amount,
                                       from_port=pump_stock_position,
                                       to_port=pump_reactor_position,
                                       velocity_ml=pump_default_velocity
                                       )
            return Say("Adding " + amount + " mils analyte into the flask")

    def dispense_titrant(self, amount):

        if amount >= 100:
            return Say('The input volume is too high.')
        else:
            self.total_solution_in_flask += int(amount)
            self.remaining_titrant_stock -= int(amount)
            self.total_volume_dispensed_titrant += int(amount)
            syringe_pump_2.dispense_ml(volume_ml=amount,
                                       from_port=pump_stock_position,
                                       to_port=pump_reactor_position,
                                       velocity_ml=pump_default_velocity
                                       )
        return Say("Adding " + amount + " mils titrant into the flask")

    def home_system(self):

        return Sequence(Prompt('What is the volume of the titrant stock solution in mils',
                               lambda amount: self.initialize_titrant_stock(amount)),
                        Prompt('What is the volume of the analyte stock solution in mils',
                               lambda amount: self.initialize_analyte_stock(amount)),
                        Prompt('How much analyte is in the flask',
                               lambda amount: self.initialize_analyte(amount)),
                        self.prime_syringe_pump
                        ),

    def initialize_titrant_stock(self, amount):
        self.remaining_titrant_stock = int(amount)

    def initialize_analyte(self, amount):
        self.total_solution_in_flask = int(amount)

    def initialize_analyte_stock(self, amount):
        self.remaining_analyte_stock = int(amount)

    def end_titration(self):
        return Say("The total volume of titrant dispensed is " +
                   str(self.total_volume_dispensed_titrant) + " mils." + "\n"
                                                                         "The total volume of analyte dispensed is " +
                   str(self.total_volume_dispensed_analyte) + " mils." "\n"
                                                              'Would you like to start a new titration?')

    def report_status(self):
        return Say("There are " + str(self.remaining_titrant_stock) + " mils titrant stock solution" + " and " +
                   str(self.remaining_analyte_stock) + " mils analyte stock solution." "\n"
                                                       "The total volume of solution in the flask is " + str(
            self.total_solution_in_flask) + " mils.")

    def start_stirring(self):
        stir_plate.start_stirring()
        stir_plate.target_stir_rate = 250

    # the stir plate stops stirring
    def stop_stirring(self):
        stir_plate.stop_stirring()

    def change_stir_rate(command):
        max_stir_rate = 1000
        if command == 'decrease':
            if stir_plate.target_stir_rate <= 75:
                stir_plate.target_stir_rate = stir_plate.target_stir_rate
            else:
                stir_plate.target_stir_rate = stir_plate.target_stir_rate / 2
        if command == 'increase':
            if stir_plate.target_stir_rate >= max_stir_rate / 2:
                stir_plate.target_stir_rate = (stir_plate.target_stir_rate
                                               + max_stir_rate) / 2
            else:
                stir_plate.target_stir_rate = stir_plate.target_stir_rate * 2



    def new_titration(self):
        self.total_solution_in_flask = 0
        self.total_volume_dispensed_titrant = 0
        self.total_volume_dispensed_analyte = 0
        return Say("starting rinsing cycle")

    def no_new_titration(self):

        return Say("ending experiment. Please clean up and say \n quit \n to terminate Jarvis")

    def confirmation(self, answer):
        return Listen({
            'yes': Sequence(self.new_titration, self.exit_loop),
            'no': Sequence(self.no_new_titration, self.exit_loop)
        })

    def command_listen(self):


        return Listen({
            '(.* )?status': self.report_status,

            'start stirring': self.start_stirring,

            'quit': self.exit_loop,

            'home system': Sequence(Prompt('What is the volume of the titrant stock solution in mils',
                                           lambda amount: self.initialize_titrant_stock(amount)),
                                    Prompt('What is the volume of the analyte stock solution in mils',
                                           lambda amount: self.initialize_analyte_stock(amount)),
                                    Prompt('How much analyte is in the flask',
                                           lambda amount: self.initialize_analyte(amount)),
                                    #todo: implement the overflow functionality
                                    ),

            'add analyte': Prompt('How many mils would you like to dispense?',
                                  lambda amount: self.dispense_analyte(amount)),

            'add titrant': Prompt('How many mils would you like to dispense?',
                                  lambda amount: self.dispense_titrant(amount)),

            'end titration': Sequence(
                self.end_titration,
                Loop(self.confirmation)),

            '(.* )?help( .*)?': self.HELP,

            'tutorial': Sequence(
                Say('Starting the tutorial'),
                Say('Please stop the pump', lambda: self.command_listen()),
                Say('Please dispense 10 milliliters', lambda: self.command_listen()),
                Say('Tutorial complete!')
            )
        })

    def prompt(self):
        return Sequence(

            # Say(f'Welcome to the test control script, {self.HELP}'),
            Say(f'Welcome'),
            Loop(self.command_listen()),
            Say('Goodbye!')

        )


rpc_client = Client('ws://20.36.16.192')

# Peristaltic pump info
# pump_port_evacuate = 'COM4'  # check on your own system
# peristaltic_pump_evacuate = NewEraPeristalticPumpInterface(port=pump_port_evacuate)
peristaltic_pump_evacuate: NewEraPeristalticPumpInterface = rpc_client.get_instance('peristaltic_pump_evacuate')
# pump_port_dispense = 'COM26'  # check on your own system
# peristaltic_pump_dispense = NewEraPeristalticPumpInterface(port=pump_port_dispense)
peristaltic_pump_dispense: NewEraPeristalticPumpInterface = rpc_client.get_instance('peristaltic_pump_dispense')

# Syringe pump info


# tecan_cavro_serial_id = 'FT2YOS81'  # todo
syringe_volume = 5.0  # mL, todo
# tecan_cavro_serial = Serial(device_serial=tecan_cavro_serial_id, baudrate=9600)
# syringe_pump_1 = TecanCavro(tecan_cavro_serial, address=0, syringe_volume_ml=syringe_volume)  # todo
syringe_pump_1: TecanCavro = rpc_client.get_instance('syringe_pump_1')
syringe_pump_1.home()
# syringe_pump_2 = TecanCavro(tecan_cavro_serial, address=1, syringe_volume_ml=syringe_volume)  # todo
syringe_pump_2: TecanCavro = rpc_client.get_instance('syringe_pump_2')
syringe_pump_2.home()

pump_default_velocity = 4  # mL/min
pump_slow_velocity = 2  # mL/min

pump_waste_position = 1
pump_stock_position = 2
pump_reactor_position = 3

# Stir plate info
#
# port = 'COM27'
# stir_plate = MagneticStirrer(device_port=port)
stir_plate: MagneticStirrer = rpc_client.get_instance('stir_plate')


# Titration data
TestScript.run_script()
