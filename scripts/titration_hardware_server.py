from rpc_gateway.server import Server
from newera.new_era import NewEraPeristalticPumpInterface
from north_devices.pumps.tecan_cavro import TecanCavro
from ftdi_serial import Serial
from ika.magnetic_stirrer import MagneticStirrer

GATEWAY_URL = 'ws://20.36.16.192'

# Peristaltic pump info
pump_port_evacuate = 'COM4'  # check on your own system
peristaltic_pump_evacuate = NewEraPeristalticPumpInterface(port=pump_port_evacuate)

pump_port_dispense = 'COM26'  # check on your own system
peristaltic_pump_dispense = NewEraPeristalticPumpInterface(port=pump_port_dispense)

# Syringe pump info
tecan_cavro_serial_id = 'FT2YOS81'  # todo
syringe_volume = 5.0  # mL, todo
tecan_cavro_serial = Serial(device_serial=tecan_cavro_serial_id, baudrate=9600)
syringe_pump_1 = TecanCavro(tecan_cavro_serial, address=0, syringe_volume_ml=syringe_volume)  # todo
# syringe_pump_1.home()
syringe_pump_2 = TecanCavro(tecan_cavro_serial, address=1, syringe_volume_ml=syringe_volume)  # todo
# syringe_pump_2.home()

# Stir plate info
#
port = 'COM27'
stir_plate = MagneticStirrer(device_port=port)


# register hardware with gateway
rpc_server = Server(GATEWAY_URL)
rpc_server.register_instance('peristaltic_pump_evacuate', peristaltic_pump_evacuate)
rpc_server.register_instance('peristaltic_pump_dispense', peristaltic_pump_dispense)
rpc_server.register_instance('syringe_pump_1', syringe_pump_1)
rpc_server.register_instance('syringe_pump_2', syringe_pump_2)
rpc_server.register_instance('stir_plate', stir_plate)

rpc_server.start()
